package com.ikomet.shopping;

import com.ikomet.adapter.CardAdapter;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;



public class TextileList extends ActionBarActivity {

	 RecyclerView mRecyclerView;
	 RecyclerView.LayoutManager mLayoutManager;
	 RecyclerView.Adapter mAdapter;
	 private Toolbar mToolbar;
	  
	 @Override
	 protected void onCreate(Bundle savedInstanceState) {
	 super.onCreate(savedInstanceState);
	 setContentView(R.layout.activity_textile);
	/* mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("Textile Shopping");*/
	 mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
	 mRecyclerView.setHasFixedSize(true);
	  
	 mLayoutManager = new LinearLayoutManager(this);
	 mRecyclerView.setLayoutManager(mLayoutManager);
	  
	 mAdapter = new CardAdapter();
	 mRecyclerView.setAdapter(mAdapter);
	  
	 }
	  
	  
	 @Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	 // Inflate the menu; this adds items to the action bar if it is present.
	 getMenuInflater().inflate(R.menu.main, menu);
	 return true;
	 }
	  
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	 // Handle action bar item clicks here. The action bar will
	 // automatically handle clicks on the Home/Up button, so long
	 // as you specify a parent activity in AndroidManifest.xml.
	 int id = item.getItemId();
	  
	 //noinspection SimplifiableIfStatement
	 if (id == R.id.action_settings) {
	 return true;
	 }
	  
	 return super.onOptionsItemSelected(item);
	 }
	 
}
