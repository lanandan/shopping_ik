package com.ikomet.shopping;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends Activity{

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		
		 new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent loginIntent = new Intent(SplashScreenActivity.this,
							SignInActivity.class);
					loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(loginIntent);
					overridePendingTransition(R.anim.right_in, R.anim.left_out);

				}
			}, 3000);
	}
}
