package com.ikomet.shopping;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailedActivity extends ActionBarActivity {

	ImageView prod_img,chat,favourite;
	TextView price,prod_name,prod_description;
	private static final String SH_UIDS = "Tripuid";	
	private static final String PRODUCTNAME = "productname";
	private static final String DESCRIPTION = "description";
	private static final String PRICE = "price";	
	SharedPreferences shared;
	String strprdname,strprddesc,strprdprice,strdimage;
	private Toolbar mToolbar;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detailed);
		/*mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("Textile Shopping")*/;
		prod_img=(ImageView)findViewById(R.id.prdimg);
		price=(TextView)findViewById(R.id.pricedetails);
		prod_name=(TextView)findViewById(R.id.prdname);
		prod_description=(TextView)findViewById(R.id.description);
		chat=(ImageView)findViewById(R.id.imgchat);
		favourite=(ImageView)findViewById(R.id.imgfav);
		
		 shared = getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		 strprdname=(shared.getString(PRODUCTNAME,null));
		 strprddesc=(shared.getString(DESCRIPTION,null));
		 strprdprice=(shared.getString(PRICE,null));
		 strdimage=(shared.getString("image",null));

		 Log.e("Shared preference",strprdname+","+strprdprice+","+strprddesc);
		 
		 
		 prod_name.setText(strprdname);
		 prod_description.setText(strprddesc);
		 price.setText(strprdprice);
		 
		 if(strdimage.equals("ONE"))
		 {
			 prod_img.setImageResource(R.drawable.f1);
			 
		 }
		 if(strdimage.equals("TWO"))
		 {
			 prod_img.setImageResource(R.drawable.f2);
			 
		 }
		 if(strdimage.equals("THREE"))
		 {
			 prod_img.setImageResource(R.drawable.f3);
			 
		 }
		 if(strdimage.equals("FOUR"))
		 {
			 prod_img.setImageResource(R.drawable.s1);
			 
		 }
		 if(strdimage.equals("FIVE"))
		 {
			 prod_img.setImageResource(R.drawable.s2);
			 
		 }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detailed, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
