package com.ikomet.shopping;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignInActivity extends Activity{

	
	Button btn_singin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		
		btn_singin=(Button)findViewById(R.id.btn_singin);
		btn_singin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent loginIntent = new Intent(SignInActivity.this,
						MainActivity.class);
				
				startActivity(loginIntent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
}
