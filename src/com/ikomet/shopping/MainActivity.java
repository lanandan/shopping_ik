package com.ikomet.shopping;





import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {

	
	ViewFlipper viewFlipper;
	public boolean input=false;
	GridView gr;
	private float lastx;
	LinearLayout ll1,ll2,ll3,ll4,ll5,ll6,ll7,ll8,ll9;
	
	ImageView img_discount,img_newarrival;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		viewFlipper=(ViewFlipper)findViewById(R.id.view_flipper);
        if(input==false)
        viewFlipper.startFlipping();
        
        img_discount=(ImageView)findViewById(R.id.img_discount);
        ll1=(LinearLayout)findViewById(R.id.ll1);
        ll2=(LinearLayout)findViewById(R.id.ll2);
        ll3=(LinearLayout)findViewById(R.id.ll3);
        ll4=(LinearLayout)findViewById(R.id.ll4);
        ll5=(LinearLayout)findViewById(R.id.ll5);
        ll6=(LinearLayout)findViewById(R.id.ll6);
        ll7=(LinearLayout)findViewById(R.id.ll7);
        ll8=(LinearLayout)findViewById(R.id.ll8);
        ll9=(LinearLayout)findViewById(R.id.ll9);
        
        ll1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent lllinear=new Intent(MainActivity.this, TextileList.class);
				startActivity(lllinear);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
        
        
        img_newarrival=(ImageView)findViewById(R.id.img_newarrival);
        
        img_discount.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent discount=new Intent(MainActivity.this,DiscountActivity.class );
				startActivity(discount);
			}
		});
        
        img_newarrival.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
        
		
	}

	
	public boolean onTouchEvent(MotionEvent touchevent)
	{	//viewFlipper.stopFlipping();
		input=true;
		 viewFlipper.startFlipping();
		switch (touchevent.getAction())
		{
	case MotionEvent.ACTION_DOWN:
		lastx=touchevent.getX();
		//Toast.makeText(FliperMainActivity.this,"actiondown"+lastx+"" , Toast.LENGTH_SHORT).show();

		break;
		
	case MotionEvent.ACTION_UP:
		
	{
		 float currentX = touchevent.getX();

	//Toast.makeText(FliperMainActivity.this,"actionup"+currentX+"" , Toast.LENGTH_SHORT).show();
		if(lastx<currentX)
		{
			 
		 // set the required Animation type to ViewFlipper
	    // The Next screen will come in form Left and current Screen will go OUT from Right
	    viewFlipper.setInAnimation(MainActivity.this, R.anim.in_from_left);
	    viewFlipper.setInAnimation(MainActivity.this, R.anim.out_to_right);
	    // Show the next Screen
	    viewFlipper.showNext();
		}
		 // if right to left swipe on screen
	    if (lastx > currentX)
	    {
	       
	        // set the required Animation type to ViewFlipper
	        // The Next screen will come in form Right and current Screen will go OUT from Left
	       viewFlipper.setInAnimation(MainActivity.this, R.anim.in_from_right);
	        viewFlipper.setInAnimation(MainActivity.this, R.anim.out_to_left);
	        // Show The Previous Screen
	        viewFlipper.showPrevious();
	    }

	    break;
	    }
	}
		return false;
		}
	
}
