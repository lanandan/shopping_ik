package com.ikomet.bean;

public class NatureItem {

	private String mName;
	private String mDes;
	private int mThumbnail;
	private int addimg;
	private String price;

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getDes() {
		return mDes;
	}

	public void setDes(String des) {
		this.mDes = des;
	}

	public int getThumbnail() {
		return mThumbnail;
	}

	public void setThumbnail(int thumbnail) {
		this.mThumbnail = thumbnail;
	}

	public String getprice() {
		return price;
	}

	public void setprice(String price) {
		this.price = price;
	}

	public void setaddbtn(int addimg) {
		this.addimg = addimg;
	}

	public int getaddbtn() {
		return addimg;
	}

}