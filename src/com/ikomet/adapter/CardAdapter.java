package com.ikomet.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.ikomet.bean.NatureItem;
import com.ikomet.shopping.DetailedActivity;
import com.ikomet.shopping.R;

/**
 * Created by Edwin on 18/01/2015.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

	private static final String SH_UIDS = "Tripuid";
	private static final String PRODUCTNAME = "productname";
	private static final String DESCRIPTION = "description";
	private static final String PRICE = "price";
	SharedPreferences shared;
	List<NatureItem> mItems;

	public CardAdapter() {
		super();

		mItems = new ArrayList<NatureItem>();
		NatureItem nature = new NatureItem();

		nature.setName("Raymonds Wolf");
		nature.setDes("This is a court feel pride to be the product of raymonds.");
		nature.setThumbnail(R.drawable.f1);
		nature.setaddbtn(R.drawable.add);
		nature.setprice("250$");

		mItems.add(nature);

		nature = new NatureItem();

		nature.setName("Court");
		nature.setDes("This is a product of Court shirt feel pride to let into the public about Court shirt");
		nature.setThumbnail(R.drawable.f2);
		nature.setaddbtn(R.drawable.add);
		nature.setprice("450$");

		mItems.add(nature);

		nature = new NatureItem();

		nature.setName("Court and Shoot");
		nature.setDes("This is a Pant which comes under the product of titanium and court very gentel black");
		nature.setThumbnail(R.drawable.f3);
		nature.setaddbtn(R.drawable.add);
		nature.setprice("650$");

		mItems.add(nature);

		nature = new NatureItem();

		nature.setName("Iguazu Falls Red Sareer");
		nature.setDes("Red Sareer Made in Pure Febric");
		nature.setThumbnail(R.drawable.s1);
		nature.setprice("700$");
		nature.setaddbtn(R.drawable.add);
		mItems.add(nature);

		nature = new NatureItem();
		nature.setName("Roganda red sareer");
		nature.setDes("Pride name Roganda makes us to feel comfortable, this is having golden frontend also. red with golden look very great");
		nature.setThumbnail(R.drawable.s2);
		nature.setprice("540$");
		nature.setaddbtn(R.drawable.add);
		mItems.add(nature);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(viewGroup.getContext()).inflate(
				R.layout.recycler_view_card_item, viewGroup, false);
		ViewHolder viewHolder = new ViewHolder(v);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int i) {
		NatureItem nature = mItems.get(i);
		viewHolder.tvNature.setText(nature.getName());
		viewHolder.tvDesNature.setText(nature.getDes());
		viewHolder.imgThumbnail.setImageResource(nature.getThumbnail());
		viewHolder.btnadd.setImageResource(nature.getaddbtn());
		viewHolder.rupees.setText(nature.getprice());

	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

		public ImageView imgThumbnail;
		public TextView tvNature;
		public TextView tvDesNature;
		public ImageView btnadd;
		public TextView rupees;
		private Context context;

		public ViewHolder(View itemView) {
			super(itemView);
			context = itemView.getContext();
			imgThumbnail = (ImageView) itemView
					.findViewById(R.id.img_thumbnail);
			tvNature = (TextView) itemView.findViewById(R.id.tv_nature);
			tvDesNature = (TextView) itemView.findViewById(R.id.tv_des_nature);
			rupees = (TextView) itemView.findViewById(R.id.txtprice);
			btnadd = (ImageView) itemView.findViewById(R.id.imgadd);
			btnadd.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.imgadd) {

				shared = context.getSharedPreferences(SH_UIDS,
						Context.MODE_PRIVATE);
				Editor editor = shared.edit();
				editor.clear();
				editor.putString(PRODUCTNAME, tvNature.getText().toString());
				editor.putString(DESCRIPTION, tvDesNature.getText().toString());
				editor.putString(PRICE, rupees.getText().toString());

				if (tvNature.getText().toString().equals("Raymonds Wolf")) {
					editor.putString("image", "ONE");
				}

				if (tvNature.getText().toString().equals("Court")) {
					editor.putString("image", "TWO");
				}

				if (tvNature.getText().toString().equals("Court and Shoot")) {
					editor.putString("image", "THREE");
				}

				if (tvNature.getText().toString()
						.equals("Iguazu Falls Red Sareer")) {
					editor.putString("image", "FOUR");
				}

				if (tvNature.getText().toString().equals("Roganda red sareer")) {
					editor.putString("image", "FIVE");
				}

				editor.commit();
				Intent intent = new Intent(context, DetailedActivity.class);
				context.startActivity(intent);
			}
		}
	}
}